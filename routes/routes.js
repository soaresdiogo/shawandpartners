var appRouter = function(app) {
    app.get("/:word", function(req, res) {
        if(isPalindrome(req.params.word)){
            res.status(200).json({
                message: 'IS A PALINDROME'
            });
        }else{
            res.status(400).json({
                message: 'IS NOT A PALINDROME'
            });
        }
    });
}

var isPalindrome = function(word){
    var medium = word.length / 2;
    for (var i = 0; i < medium; i++) {
        if (word.charAt(i) != word.charAt((word.length -1) - i)) {
            return false;
        }
    }
    return true;
}

module.exports = appRouter;