var api = require('nodeunit-httpclient').create({
    port: 3000,
    path: '/',
    status: 200,
    headers: { 
        'content-type': 'application/json'
    }
});
var http = require('http');
exports.request = {
    'GET': function(test) {
        console.log(test);
        var server1 = http.createServer(function(req, res) {
            test.equal(req.method, 'GET');
            test.equal(req.url, '/ABA');
            
            res.writeHead(200, {'content-type': 'text/plain'});
            res.end('success');
        });
        server1.listen(3000, '127.0.0.1');
    },
    'GET with 404 no content': function(test) {
        console.log(test);
        var server1 = http.createServer(function(req, res) {
            test.equal(req.method, 'GET');
            test.equal(req.url, '/DIOGO');
            
            res.writeHead(400, {'content-type': 'text/plain'});
            res.end('fail');
        });
        server1.listen(3000, '127.0.0.1');
    }
}
